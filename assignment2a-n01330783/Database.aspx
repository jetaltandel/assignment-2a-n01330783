﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="assignment2a_n01330783.Database" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2>Database table join</h2>
   <p>In a database there are different types of tables joining like innerjoin, leftjoin, rightjoin, fulljoin and sefljoin.when you want some informatoin 
       from different table you can access other table information using different types of table joining.For me selfjoining table is the difficult to understand.
   </p>
</asp:Content>
<asp:Content  ContentPlaceHolderID="DetailContent" runat="server">
    
   

    <h2>Code For Creating Table:</h2>
    <p>CREATE TABLE Employee<br/>( Employee_no   Number(6)   PRIMARY KEY,<br/>Employee_name   VARCHAR2(10),<br/>Hire_date   DATETIME,
        <br/>Employee_dept  VARCHAR2(10))</p>
    <p> The above code create Employee table with four columns for Employee_no, Employee_name, Hire_date and Employee_dept. For Employee table Employee_no is 
        the primary key where data type is number, whereas Employee_name and Employee_dept are variable character type datatype, Hire_date as DATETIME datatype. </p>
    
      
</asp:Content>



<asp:Content ContentPlaceHolderID="ExampleContent" runat="server">
    <h2>Code for Innerjoin</h2>
    
    <p>SELECT order_date,order_amount<br/>FROM customers<br/>JOIN orders<br/>ON customers.customer_id=orders.customer_id<br/>
        WHERE customer_id=3
    </p>
    <p>The above code select two columns, one for order_date and other for order_amount(where customer_id is 3). Here customers table innerjoin with orders table,for innerjoin customer_id
        of customer table match with customer_id of orders table, it's consider only matching data.
    </p>
    <a href="http://www.sql-join.com/">Link I Found</a>

</asp:Content>

<asp:Content ContentPlaceHolderID="LinkContent" runat="server">

    <h2>Useful links</h2>
    <ul>
        <li><a href="https://www.w3schools.com/sql/">W3school</a></li>
        <li><a href="https://www.quackit.com/database/tutorial/">Quackit</a></li>
        <li><a href="http://www.sqlcourse.com/">Sqlcourse</a></li>
    </ul>
</asp:Content>
