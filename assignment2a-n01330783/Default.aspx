﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="assignment2a_n01330783._Default" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

    <div class="jumbotron">                                      <!--Reference:jumbotron by default class-->
        <h1>Hello Humber Students:</h1>
        <p>I am a student of web development course.</p>
     </div>
    
<div class="row">

    <div class="col-md-4">
    <h2>Database:</h2>
    <p>Database is collection of information. You can easily get information from a database and you can update it whenever you want.</p>
    </div>


   <div class="col-md-4">
     <h2>Digital Design:</h2>
      <p>Digital Design is most about front end, which include a main concept of HTML, CSS and photoshop.</p>
    </div>

    <div class="col-md-4">
    <h2>Web programming:</h2>
      <p>One of the most popular language for web programming is javascript.</p>
    </div>
</div>
</asp:Content>
