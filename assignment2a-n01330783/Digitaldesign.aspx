﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Digitaldesign.aspx.cs" Inherits="assignment2a_n01330783.Digitaldesign" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
   
    <h2>Digital Design:</h2>
     <p>Digital design course is all about design front page. Using photoshop we can create some interactive page. It might be difficult for user who has been using
         photoshop for the first time. It needs some practice to dragged and dropped image at perfect place.
     </p>
      
    
</asp:Content> 
<asp:Content ContentPlaceHolderID="DetailContent" runat="server">
    
    <h2>Basic of HTML</h2>
    <p>&lt;h1&gt; Customer Information: &lt;/h1&gt;<br/>
        &lt;form  action=&quot;#&quot; method=&quot;post&quot; &gt;<br/>
        &lt;div&gt;<br/>
        &lt;lable  for=&quot;fname&quot;&gt;First Name:&lt;/lable&gt;<br/>
        &lt;input  type=&quot;text&quot; id=&quot;fname&quot; name=&quot;firstname&quot; &gt;<br/>
        &lt;/div&gt;<br/>
        &lt;div&gt;<br/>
        &lt;lable  for=&quot;lname&quot;&gt;Last Name:&lt;/lable&gt;<br/>
        &lt;input  type=&quot;text&quot; id=&quot;lname&quot; name=&quot;lastname&quot; &gt;<br/>
        &lt;/div&gt;<br/>
        &lt;div&gt;<br/>
        &lt;button  type=&quot;submit&quot; id=&quot;Submit&quot; name=&quot;submitbutton&quot; &gt;Submit&lt;/button&gt;<br/>
        &lt;/div&gt;<br/>
         &lt;/form&gt;
    </p>
    <p>In above example form used for customer information,Form allow user to enter their data.Here user required their first name and last name to submit form. </p>
    
</asp:Content>





<asp:Content ContentPlaceHolderID="ExampleContent" runat="server">
    <h2>Code for layout</h2>
    <p>Useful tags for designing layout.<br/> &lt;header&gt; : header tag used to define a header of the page and only one header in the document<br/>
       &lt;nav&gt;: Navigation tag used to define navigation link of page.<br/>&lt;section&gt;: Section tag used to define a section of the page we can include more than one section on the page
        <br/> &lt;article&gt;:article tag used to define some related content to the main content.<br/>&lt;footer&gt;:footer tag define footer of the page.<br/>&lt;main&gt;:main tag used to define 
        main content of page, only one main section  in the document.<br/>&lt;aside&gt; :aside tag define content not directly related to main content.
    </p>
    <a href="https://www.w3schools.com/html/html_layout.asp">Link I Found</a><br/>
    
   
    
</asp:Content>



<asp:Content ContentPlaceHolderID="LinkContent" runat="server">

    <h2>Useful Links</h2>
    <ul>
        <li><a href="https://www.w3schools.com/sql/">W3school</a></li>
        <li><a href="https://html-css-js.com/html/">HTML</a></li>
        <li><a href="https://css-tricks.com/">CSS Trick</a></li>
    </ul>

</asp:Content>