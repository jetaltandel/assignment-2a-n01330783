﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Webprogramming.aspx.cs" Inherits="assignment2a_n01330783.Webprogramming" %>


<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    <h2>JavaScript:</h2>
     <p>JavaScript is the most popular language for web programming. There are some loops for checking condition example like if, if else, for and
         while loops, while loop will continue to run as long as the condition is true. It will stop when the condition become false. In my opinion
         to understanding between for loop and while loop is bit of confusing.

     </p>
</asp:Content>
<asp:Content ContentPlaceHolderID="DetailContent" runat="server">
    
    <h2>Array Example:</h2>
    <p> var student = ["jetal","subham","jinish"];<br/>alert("welcome:"+student[0]);<br/>console.log(student[ ]);</p>
    <p>In above example first variable declared for array.Here array name is student and it's values declared in [ ] braces. When code will run it's popup messase
        say welcome:jetal and console used for hold the student array.
    </p>
        
       
</asp:Content>



<asp:Content ContentPlaceHolderID="ExampleContent" runat="server">
    <h2>Code for loop</h2>
    
    <p>var cars = [ "audi" , "volvo" , "BMW" ] ; <br/> var i = 0;<br/> for (i = 0 ; i <= cars.length ; i++ )<br/>{ alert (cars[i]) ;<br/>}</p>
    <p>Here cars array is declared first with its values,for loop will run 3 times. When for loop will run for First time popup message show audi then 
        volvo and last it will show BMW.</p>
    <a href="https://www.w3schools.com/js/js_loop_for.asp">Link I Found</a><br/>
</asp:Content>
<asp:Content ContentPlaceHolderID="LinkContent" runat="server">
    <h2>Useful Links</h2>
    <ul>
        <li><a href="https://www.w3schools.com/sql/">W3school</a></li>
        <li><a href="https://www.javatpoint.com/javascript-tutorial">Javatpoint</a></li>
        <li><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript">MDN</a></li>
    </ul>
</asp:Content>